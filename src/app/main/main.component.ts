import { Component, OnInit } from "@angular/core";
import "rxjs/add/operator/map";
import { Headers, Http, Response, RequestOptions } from "@angular/http";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  //initialize variables
  title = "darey";
  authors: any[];
  coverimage;
  description;
  finaldesc;
  gotten;
  found = false;
  ISBN;
  error = false;
  networkstatus = false;
  books;

  results: any[];

  constructor(private http: Http) {
    this.http = http;
  }

  ngOnInit() {
  }


  // pass isbn from template
  number(code) {
    // console.log(code.value);
    Number;
    this.ISBN = code.value;
    this.go(this.ISBN);
  }


  //get first 10 words from description
  shortdesc(word) {
    let str2 = word.replace(/(([^\s]+\s\s*){10})(.*)/, "$1");
    return str2;
  }

  //get data from API
  go(isbnNumber) {
    let header = new Headers({ 'Authorization': 'AIzaSyCmwD5gq2i4VafwoMbbeFkEWXHr5dTP0S8' });
    const options = new RequestOptions({
      headers: header,
    });
    this.http
      .get("https://www.googleapis.com/books/v1/volumes?q=" + isbnNumber, { headers: header })
      .map((response: Response) => response.json())
      .subscribe(
        res => {
          console.log(res.items);
          this.books = res.items;
        },
        err => {
          // let error = JSON.parse(err.text());
          // console.log(err);
          // console.log(err._body);
          this.error = true;

          return false;
        }
      );

  }
}
